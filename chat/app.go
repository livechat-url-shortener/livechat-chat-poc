package chat

import (
	"net/http"

	"github.com/go-playground/validator/v10"
	"gitlab.com/livechat-url-shortener/livechat-chat-poc/chat/adapters"
)

type ChatApplication interface {
	MountEndpoint() http.HandlerFunc
}

func CreateApplication(v *validator.Validate) ChatApplication {
	adapter := adapters.NewWebSocketAdapter(v)

	return &app{
		wsAdapter: adapter,
	}
}

type app struct {
	wsAdapter adapters.WebSocketAdapter
}

func (a *app) MountEndpoint() http.HandlerFunc {
	return a.wsAdapter.Listen
}
