package adapters

import "fmt"

const (
	createChannelAction = "create channel"
	joinChannelAction   = "join channel"
	leaveChannelAction  = "leave channel"
	sendMessageAction   = "send message"
)

const (
	channelCreatedEvent  = "channel created"
	joinedChannelEvent   = "joined channel"
	leftChannelEvent     = "left channel"
	receivedMessageEvent = "received message"
	errorEvent           = "error"
)

type incomingMessage struct {
	Action  string `json:"action" validator:"oneof='create channel' 'join channel' 'leave channel' 'send message'"`
	Content string `json:"content"`
}

type outgoingMessage struct {
	Action  string `json:"action"`
	Content string `json:"content"`
}

func createChannelCreatedEvent(channelName string) outgoingMessage {
	return outgoingMessage{Action: channelCreatedEvent, Content: channelName}
}

func createJoinedChannelEvent(channelName string) outgoingMessage {
	return outgoingMessage{Action: joinedChannelEvent, Content: channelName}
}

func createLeftChannelEvent(channelName string) outgoingMessage {
	return outgoingMessage{Action: leftChannelEvent, Content: channelName}
}

func createReceivedMessageEvent(channelName string, message string) outgoingMessage {
	return outgoingMessage{
		Action:  receivedMessageEvent,
		Content: fmt.Sprintf("%s:%s", channelName, message),
	}
}

func createErrorEvent(err error) outgoingMessage {
	return outgoingMessage{Action: errorEvent, Content: err.Error()}
}
