package adapters

import (
	"errors"
	"fmt"
	"sync"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
)

type websocketUser struct {
	conn  *websocket.Conn
	uuid  string
	mutex sync.Mutex
}

func (u *websocketUser) WriteJSON(v interface{}) error {
	u.mutex.Lock()
	defer u.mutex.Unlock()

	return u.conn.WriteJSON(v)
}

type websocketClients struct {
	Channels  map[string]*websocketChannel
	Broadcast chan incomingMessage
}

type websocketChannel struct {
	mutex   sync.Mutex
	Clients map[string]*websocketUser
}

func newWebSocketClient() *websocketClients {
	return &websocketClients{
		Channels: make(map[string]*websocketChannel),
	}
}

func (w *websocketClients) CreateChannel(firstConn *websocketUser) (string, error) {
	channelName := uuid.New().String()
	w.Channels[channelName] = &websocketChannel{
		Clients: make(map[string]*websocketUser),
		mutex:   sync.Mutex{},
	}
	w.Channels[channelName].Clients[firstConn.uuid] = firstConn

	return channelName, nil
}

func (w *websocketClients) JoinChannel(channelName string, conn *websocketUser) error {
	isInChannel, err := w.userIsInTheChannel(channelName, conn)
	if err != nil {
		return err
	}

	if isInChannel {
		return errors.New("You're already in this channel")
	}

	w.Channels[channelName].mutex.Lock()
	w.Channels[channelName].Clients[conn.uuid] = conn
	w.Channels[channelName].mutex.Unlock()

	return nil
}

func (w *websocketClients) ExitChannel(channelName string, conn *websocketUser) error {
	isInChannel, err := w.userIsInTheChannel(channelName, conn)
	if err != nil {
		return err
	}

	if !isInChannel {
		return errors.New("You're not in this channel")
	}

	w.Channels[channelName].mutex.Lock()
	defer w.Channels[channelName].mutex.Unlock()

	delete(w.Channels[channelName].Clients, conn.uuid)
	return nil
}

func (w *websocketClients) ForceExitUser(conn *websocketUser) {
	for uuid, ch := range w.Channels {
		w.Channels[uuid].mutex.Lock()
		defer w.Channels[uuid].mutex.Unlock()

		_, inChannel := ch.Clients[conn.uuid]
		if inChannel {
			delete(w.Channels[uuid].Clients, conn.uuid)
		}
	}
}

func (w *websocketClients) SendMessage(channelName string, message string, conn *websocketUser) error {
	w.Channels[channelName].mutex.Lock()
	defer w.Channels[channelName].mutex.Unlock()

	if _, ok := w.Channels[channelName]; !ok {
		return fmt.Errorf("channel %s does not exists", channelName)
	}
	isInChannel, err := w.userIsInTheChannel(channelName, conn)
	if err != nil {
		return err
	}

	if !isInChannel {
		return errors.New("You're not in this channel")
	}

	for _, client := range w.Channels[channelName].Clients {
		err = client.WriteJSON(websocketMessage{
			Action:  "received message",
			Content: fmt.Sprintf("%s:%s", channelName, message),
		})
		if err != nil {
			logrus.Errorf("cannot send message: %s", err)
			return err
		}
	}

	return nil
}

func (w *websocketClients) userIsInTheChannel(channelName string, conn *websocketUser) (bool, error) {
	if _, ok := w.Channels[channelName]; !ok {
		return false, fmt.Errorf("channel %s does not exists", channelName)
	}

	_, inChannel := w.Channels[channelName].Clients[conn.uuid]
	return inChannel, nil
}
