package adapters

import (
	"errors"
	"net/http"
	"strings"
	"sync"

	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
)

type WebSocketAdapter interface {
	Listen(http.ResponseWriter, *http.Request)
}

type WebSocketHandler interface {
	CreateChannel(firstConn *websocketUser) (string, error)
	JoinChannel(channelName string, conn *websocketUser) error
	ExitChannel(channelName string, conn *websocketUser) error
	ForceExitUser(conn *websocketUser)
	SendMessage(channelName string, message string, conn *websocketUser) error
}

func NewWebSocketAdapter(v *validator.Validate) WebSocketAdapter {
	upgrader := websocket.Upgrader{}
	upgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}

	clients := newWebSocketClient()

	return &websocketAdapter{
		wsChannel: &upgrader,
		clients:   clients,
		validator: v,
	}
}

type websocketAdapter struct {
	wsChannel *websocket.Upgrader
	clients   WebSocketHandler
	validator *validator.Validate
}

type websocketMessage struct {
	Action  string `json:"action"`
	Content string `json:"content"`
}

func (a *websocketAdapter) Listen(w http.ResponseWriter, r *http.Request) {
	conn, err := a.wsChannel.Upgrade(w, r, nil)
	if err != nil {
		logrus.Errorf("cannot get connection: %s", err)
		return
	}

	user := &websocketUser{
		conn:  conn,
		uuid:  uuid.New().String(),
		mutex: sync.Mutex{},
	}

	for {
		msg := incomingMessage{}
		err := conn.ReadJSON(&msg)
		if err != nil {
			logrus.Errorf("cannot read message: %s", err)
			a.clients.ForceExitUser(user)
			return
		}

		if err := a.validator.StructCtx(r.Context(), msg); err != nil {
			err = user.WriteJSON(createErrorEvent(err))
			break
		}

		switch msg.Action {
		case createChannelAction:
			channelName, err := a.clients.CreateChannel(user)
			if err != nil {
				break
			}
			err = user.WriteJSON(createChannelCreatedEvent(channelName))
		case joinChannelAction:
			err = a.clients.JoinChannel(msg.Content, user)
			if err != nil {
				break
			}
			err = user.WriteJSON(createJoinedChannelEvent(msg.Content))
		case leaveChannelAction:
			err = a.clients.ExitChannel(msg.Content, user)
			if err != nil {
				break
			}
			err = user.WriteJSON(createLeftChannelEvent(msg.Content))
		case sendMessageAction:
			msgParts := strings.Split(msg.Content, ":")
			err = a.clients.SendMessage(msgParts[0], msgParts[1], user)
		default:
			err = user.WriteJSON(createErrorEvent(errors.New("unknown action")))
		}

		if err != nil {
			logrus.Errorf("error happened: %s", err)
			user.WriteJSON(createErrorEvent(err))
		}
	}
}
