package adapters

import (
	"fmt"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/go-chi/chi"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
)

func Test_RunningChat(t *testing.T) {
	validator := validator.New()

	appServer := chi.NewMux()
	appServer.Get("/", NewWebSocketAdapter(validator).Listen)

	server := httptest.NewServer(appServer)
	ws := mustDialWS(t, fmt.Sprintf("ws%s/", strings.TrimPrefix(server.URL, "http")))

	defer server.Close()
	defer ws.Close()

	t.Run("user got response when channel is created", func(t *testing.T) {
		writeWSMessage(t, ws, incomingMessage{Action: "create channel"})

		outcomingMsg := outgoingMessage{}
		ws.ReadJSON(&outcomingMsg)

		assert.Equal(t, channelCreatedEvent, outcomingMsg.Action)
	})

	t.Run("user got error response when tries to join not existing channel", func(t *testing.T) {
		writeWSMessage(t, ws, incomingMessage{Action: "join channel", Content: "lorem-ipsum"})

		outcomingMsg := outgoingMessage{}
		ws.ReadJSON(&outcomingMsg)

		assert.Equal(t, errorEvent, outcomingMsg.Action)
	})

	t.Run("user got error response when try to join channel which he created", func(t *testing.T) {
		writeWSMessage(t, ws, incomingMessage{Action: "create channel"})

		outcomingMsg1 := outgoingMessage{}
		ws.ReadJSON(&outcomingMsg1)

		writeWSMessage(t, ws, incomingMessage{Action: "join channel", Content: outcomingMsg1.Content})

		outcomingMsg2 := outgoingMessage{}
		ws.ReadJSON(&outcomingMsg2)

		assert.Equal(t, errorEvent, outcomingMsg2.Action)
	})

	t.Run("user can join to the channel knowing only UUID of channel", func(t *testing.T) {
		temporaryWs := mustDialWS(t, fmt.Sprintf("ws%s/", strings.TrimPrefix(server.URL, "http")))
		defer temporaryWs.Close()
		writeWSMessage(t, temporaryWs, incomingMessage{Action: "create channel"})

		outcomingMsg1 := outgoingMessage{}
		temporaryWs.ReadJSON(&outcomingMsg1)

		writeWSMessage(t, ws, incomingMessage{Action: "join channel", Content: outcomingMsg1.Content})

		outcomingMsg2 := outgoingMessage{}
		ws.ReadJSON(&outcomingMsg2)

		assert.Equal(t, joinedChannelEvent, outcomingMsg2.Action)
	})

	t.Run("user can broadcast message to all participants in channel", func(t *testing.T) {
		temporaryUsers := []*websocket.Conn{
			mustDialWS(t, fmt.Sprintf("ws%s/", strings.TrimPrefix(server.URL, "http"))),
			mustDialWS(t, fmt.Sprintf("ws%s/", strings.TrimPrefix(server.URL, "http"))),
			mustDialWS(t, fmt.Sprintf("ws%s/", strings.TrimPrefix(server.URL, "http"))),
		}
		for _, u := range temporaryUsers {
			defer u.Close()
		}

		writeWSMessage(t, ws, incomingMessage{Action: "create channel"})
		outcomingMsg1 := outgoingMessage{}
		ws.ReadJSON(&outcomingMsg1)

		for _, u := range temporaryUsers {
			writeWSMessage(t, u, incomingMessage{Action: "join channel", Content: outcomingMsg1.Content})

			outcomingMsgUser := outgoingMessage{}
			u.ReadJSON(&outcomingMsgUser)
			assert.Equal(t, joinedChannelEvent, outcomingMsgUser.Action)
		}

		messageContent := fmt.Sprintf("%s:%s", outcomingMsg1.Content, "lorem ipsum dolor sit amet")
		writeWSMessage(t, ws, incomingMessage{Action: "send message", Content: messageContent})

		for _, u := range temporaryUsers {
			outcomingMsgUser := outgoingMessage{}
			u.ReadJSON(&outcomingMsgUser)

			assert.Equal(t, fmt.Sprintf("%s:%s", outcomingMsg1.Content, "lorem ipsum dolor sit amet"), outcomingMsgUser.Content)
		}
	})
}

func mustDialWS(t *testing.T, url string) *websocket.Conn {
	t.Helper()
	ws, _, err := websocket.DefaultDialer.Dial(url, nil)

	if err != nil {
		t.Fatalf("could not open a ws connection on %s %v", url, err)
	}

	return ws
}

func writeWSMessage(t *testing.T, conn *websocket.Conn, message incomingMessage) {
	t.Helper()
	if err := conn.WriteJSON(message); err != nil {
		t.Fatalf("could not send message over ws connection %v", err)
	}
}
