module gitlab.com/livechat-url-shortener/livechat-chat-poc

go 1.15

require (
	github.com/go-chi/chi v1.5.1
	github.com/go-playground/validator/v10 v10.4.1
	github.com/google/uuid v1.1.2
	github.com/gorilla/websocket v1.4.2
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.4.0
)
