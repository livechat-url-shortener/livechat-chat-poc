package main

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"
	"gitlab.com/livechat-url-shortener/livechat-chat-poc/chat"
)

func main() {
	validator := validator.New()

	chatApp := chat.CreateApplication(validator)

	server := chi.NewRouter()
	server.Get("/", chatApp.MountEndpoint())

	if err := http.ListenAndServe("localhost:8081", server); err != nil {
		logrus.Panicf("cannot start the application: %s", err)
	}
}
